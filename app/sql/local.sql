-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: ::1    Database: local
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_actionscheduler_actions`
--

DROP TABLE IF EXISTS `wp_actionscheduler_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hook` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`action_id`),
  KEY `hook` (`hook`),
  KEY `status` (`status`),
  KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  KEY `args` (`args`),
  KEY `group_id` (`group_id`),
  KEY `last_attempt_gmt` (`last_attempt_gmt`),
  KEY `claim_id_status_scheduled_date_gmt` (`claim_id`,`status`,`scheduled_date_gmt`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_actionscheduler_actions`
--

LOCK TABLES `wp_actionscheduler_actions` WRITE;
/*!40000 ALTER TABLE `wp_actionscheduler_actions` DISABLE KEYS */;
INSERT INTO `wp_actionscheduler_actions` VALUES (42,'action_scheduler/migration_hook','complete','2023-05-16 07:25:42','2023-05-16 09:25:42','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1684221942;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1684221942;}',1,1,'2023-05-16 07:25:47','2023-05-16 09:25:47',0,NULL);
INSERT INTO `wp_actionscheduler_actions` VALUES (43,'wp_mail_smtp_summary_report_email','pending','2023-05-22 12:00:00','2023-05-22 14:00:00','[null]','O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1684756800;s:18:\"\0*\0first_timestamp\";i:1684756800;s:13:\"\0*\0recurrence\";i:604800;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1684756800;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:604800;}',2,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL);
INSERT INTO `wp_actionscheduler_actions` VALUES (44,'wpforms_process_forms_locator_scan','complete','2023-05-16 08:39:08','2023-05-16 10:39:08','{\"tasks_meta_id\":1}','O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1684226348;s:18:\"\0*\0first_timestamp\";i:1684226348;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1684226348;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}',3,1,'2023-05-16 08:39:55','2023-05-16 10:39:55',0,NULL);
INSERT INTO `wp_actionscheduler_actions` VALUES (45,'wpforms_email_summaries_fetch_info_blocks','canceled','2023-05-18 09:14:15','2023-05-18 11:14:15','{\"tasks_meta_id\":null}','O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1684401255;s:18:\"\0*\0first_timestamp\";i:1684401255;s:13:\"\0*\0recurrence\";i:604800;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1684401255;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:604800;}',3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL);
INSERT INTO `wp_actionscheduler_actions` VALUES (46,'wpforms_process_forms_locator_scan','canceled','2023-05-17 08:39:55','2023-05-17 10:39:55','{\"tasks_meta_id\":1}','O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1684312795;s:18:\"\0*\0first_timestamp\";i:1684226348;s:13:\"\0*\0recurrence\";i:86400;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1684312795;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:86400;}',3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL);
INSERT INTO `wp_actionscheduler_actions` VALUES (47,'wp_mail_smtp_admin_notifications_update','complete','2023-05-16 08:39:55','2023-05-16 08:39:55','[1]','O:28:\"ActionScheduler_NullSchedule\":0:{}',2,1,'2023-05-16 08:39:56','2023-05-16 10:39:56',0,NULL);
INSERT INTO `wp_actionscheduler_actions` VALUES (48,'wpforms_admin_notifications_update','complete','2023-05-16 08:39:55','2023-05-16 08:39:55','{\"tasks_meta_id\":2}','O:28:\"ActionScheduler_NullSchedule\":0:{}',3,1,'2023-05-16 08:39:56','2023-05-16 10:39:56',0,NULL);
INSERT INTO `wp_actionscheduler_actions` VALUES (49,'wpforms_admin_addons_cache_update','canceled','2023-05-23 08:46:11','2023-05-23 10:46:11','{\"tasks_meta_id\":3}','O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1684831571;s:18:\"\0*\0first_timestamp\";i:1684831571;s:13:\"\0*\0recurrence\";i:604800;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1684831571;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:604800;}',3,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL);
INSERT INTO `wp_actionscheduler_actions` VALUES (50,'action_scheduler/migration_hook','pending','2023-05-16 09:39:56','2023-05-16 11:39:56','[]','O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1684229996;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1684229996;}',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,NULL);
/*!40000 ALTER TABLE `wp_actionscheduler_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_actionscheduler_claims`
--

DROP TABLE IF EXISTS `wp_actionscheduler_claims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_actionscheduler_claims` (
  `claim_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_created_gmt` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`claim_id`),
  KEY `date_created_gmt` (`date_created_gmt`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_actionscheduler_claims`
--

LOCK TABLES `wp_actionscheduler_claims` WRITE;
/*!40000 ALTER TABLE `wp_actionscheduler_claims` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_actionscheduler_claims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_actionscheduler_groups`
--

DROP TABLE IF EXISTS `wp_actionscheduler_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `slug` (`slug`(191))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_actionscheduler_groups`
--

LOCK TABLES `wp_actionscheduler_groups` WRITE;
/*!40000 ALTER TABLE `wp_actionscheduler_groups` DISABLE KEYS */;
INSERT INTO `wp_actionscheduler_groups` VALUES (1,'action-scheduler-migration');
INSERT INTO `wp_actionscheduler_groups` VALUES (2,'wp_mail_smtp');
INSERT INTO `wp_actionscheduler_groups` VALUES (3,'wpforms');
/*!40000 ALTER TABLE `wp_actionscheduler_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_actionscheduler_logs`
--

DROP TABLE IF EXISTS `wp_actionscheduler_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_actionscheduler_logs` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `action_id` bigint(20) unsigned NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`log_id`),
  KEY `action_id` (`action_id`),
  KEY `log_date_gmt` (`log_date_gmt`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_actionscheduler_logs`
--

LOCK TABLES `wp_actionscheduler_logs` WRITE;
/*!40000 ALTER TABLE `wp_actionscheduler_logs` DISABLE KEYS */;
INSERT INTO `wp_actionscheduler_logs` VALUES (1,42,'action created','2023-05-16 07:24:42','2023-05-16 09:24:42');
INSERT INTO `wp_actionscheduler_logs` VALUES (2,42,'action started via WP Cron','2023-05-16 07:25:47','2023-05-16 09:25:47');
INSERT INTO `wp_actionscheduler_logs` VALUES (3,42,'action complete via WP Cron','2023-05-16 07:25:47','2023-05-16 09:25:47');
INSERT INTO `wp_actionscheduler_logs` VALUES (4,43,'action created','2023-05-16 07:25:48','2023-05-16 09:25:48');
INSERT INTO `wp_actionscheduler_logs` VALUES (5,44,'action created','2023-05-16 08:39:08','2023-05-16 10:39:08');
INSERT INTO `wp_actionscheduler_logs` VALUES (6,45,'action created','2023-05-16 08:39:08','2023-05-16 10:39:08');
INSERT INTO `wp_actionscheduler_logs` VALUES (7,44,'action started via WP Cron','2023-05-16 08:39:55','2023-05-16 10:39:55');
INSERT INTO `wp_actionscheduler_logs` VALUES (8,44,'action complete via WP Cron','2023-05-16 08:39:55','2023-05-16 10:39:55');
INSERT INTO `wp_actionscheduler_logs` VALUES (9,46,'action created','2023-05-16 08:39:55','2023-05-16 10:39:55');
INSERT INTO `wp_actionscheduler_logs` VALUES (10,47,'action created','2023-05-16 08:39:55','2023-05-16 10:39:55');
INSERT INTO `wp_actionscheduler_logs` VALUES (11,48,'action created','2023-05-16 08:39:55','2023-05-16 10:39:55');
INSERT INTO `wp_actionscheduler_logs` VALUES (12,47,'action started via Async Request','2023-05-16 08:39:56','2023-05-16 10:39:56');
INSERT INTO `wp_actionscheduler_logs` VALUES (13,47,'action complete via Async Request','2023-05-16 08:39:56','2023-05-16 10:39:56');
INSERT INTO `wp_actionscheduler_logs` VALUES (14,48,'action started via Async Request','2023-05-16 08:39:56','2023-05-16 10:39:56');
INSERT INTO `wp_actionscheduler_logs` VALUES (15,48,'action complete via Async Request','2023-05-16 08:39:56','2023-05-16 10:39:56');
INSERT INTO `wp_actionscheduler_logs` VALUES (16,49,'action created','2023-05-16 08:46:11','2023-05-16 10:46:11');
INSERT INTO `wp_actionscheduler_logs` VALUES (17,50,'action created','2023-05-16 09:38:56','2023-05-16 11:38:56');
INSERT INTO `wp_actionscheduler_logs` VALUES (18,45,'action canceled','2023-05-16 09:39:22','2023-05-16 11:39:22');
INSERT INTO `wp_actionscheduler_logs` VALUES (19,46,'action canceled','2023-05-16 09:39:22','2023-05-16 11:39:22');
INSERT INTO `wp_actionscheduler_logs` VALUES (20,49,'action canceled','2023-05-16 09:39:22','2023-05-16 11:39:22');
INSERT INTO `wp_actionscheduler_logs` VALUES (21,45,'action canceled','2023-05-16 09:39:22','2023-05-16 11:39:22');
INSERT INTO `wp_actionscheduler_logs` VALUES (22,46,'action canceled','2023-05-16 09:39:22','2023-05-16 11:39:22');
INSERT INTO `wp_actionscheduler_logs` VALUES (23,49,'action canceled','2023-05-16 09:39:22','2023-05-16 11:39:22');
/*!40000 ALTER TABLE `wp_actionscheduler_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
INSERT INTO `wp_comments` VALUES (1,1,'Un commentateur ou commentatrice WordPress','wapuu@wordpress.example','https://fr.wordpress.org/','','2023-05-09 09:34:26','2023-05-09 07:34:26','Bonjour, ceci est un commentaire.\nPour débuter avec la modération, la modification et la suppression de commentaires, veuillez visiter l’écran des Commentaires dans le Tableau de bord.\nLes avatars des personnes qui commentent arrivent depuis <a href=\"https://fr.gravatar.com/\">Gravatar</a>.',0,'1','','comment',0,0);
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=698 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` VALUES (1,'siteurl','http://localhost:10016','yes');
INSERT INTO `wp_options` VALUES (2,'home','http://localhost:10016','yes');
INSERT INTO `wp_options` VALUES (3,'blogname','testdevwp','yes');
INSERT INTO `wp_options` VALUES (4,'blogdescription','','yes');
INSERT INTO `wp_options` VALUES (5,'users_can_register','0','yes');
INSERT INTO `wp_options` VALUES (6,'admin_email','yasmine.radouani@outlook.fr','yes');
INSERT INTO `wp_options` VALUES (7,'start_of_week','1','yes');
INSERT INTO `wp_options` VALUES (8,'use_balanceTags','0','yes');
INSERT INTO `wp_options` VALUES (9,'use_smilies','1','yes');
INSERT INTO `wp_options` VALUES (10,'require_name_email','1','yes');
INSERT INTO `wp_options` VALUES (11,'comments_notify','1','yes');
INSERT INTO `wp_options` VALUES (12,'posts_per_rss','10','yes');
INSERT INTO `wp_options` VALUES (13,'rss_use_excerpt','0','yes');
INSERT INTO `wp_options` VALUES (14,'mailserver_url','mail.example.com','yes');
INSERT INTO `wp_options` VALUES (15,'mailserver_login','login@example.com','yes');
INSERT INTO `wp_options` VALUES (16,'mailserver_pass','password','yes');
INSERT INTO `wp_options` VALUES (17,'mailserver_port','110','yes');
INSERT INTO `wp_options` VALUES (18,'default_category','1','yes');
INSERT INTO `wp_options` VALUES (19,'default_comment_status','open','yes');
INSERT INTO `wp_options` VALUES (20,'default_ping_status','open','yes');
INSERT INTO `wp_options` VALUES (21,'default_pingback_flag','1','yes');
INSERT INTO `wp_options` VALUES (22,'posts_per_page','10','yes');
INSERT INTO `wp_options` VALUES (23,'date_format','j F Y','yes');
INSERT INTO `wp_options` VALUES (24,'time_format','G\\hi','yes');
INSERT INTO `wp_options` VALUES (25,'links_updated_date_format','d F Y G\\hi','yes');
INSERT INTO `wp_options` VALUES (26,'comment_moderation','0','yes');
INSERT INTO `wp_options` VALUES (27,'moderation_notify','1','yes');
INSERT INTO `wp_options` VALUES (28,'permalink_structure','/%year%/%monthnum%/%day%/%postname%/','yes');
INSERT INTO `wp_options` VALUES (29,'rewrite_rules','a:119:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"services-types/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?services_types=$matches[1]&feed=$matches[2]\";s:50:\"services-types/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?services_types=$matches[1]&feed=$matches[2]\";s:31:\"services-types/([^/]+)/embed/?$\";s:47:\"index.php?services_types=$matches[1]&embed=true\";s:43:\"services-types/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?services_types=$matches[1]&paged=$matches[2]\";s:25:\"services-types/([^/]+)/?$\";s:36:\"index.php?services_types=$matches[1]\";s:36:\"services/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"services/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"services/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"services/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"services/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"services/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"services/([^/]+)/embed/?$\";s:41:\"index.php?services=$matches[1]&embed=true\";s:29:\"services/([^/]+)/trackback/?$\";s:35:\"index.php?services=$matches[1]&tb=1\";s:37:\"services/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?services=$matches[1]&paged=$matches[2]\";s:44:\"services/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?services=$matches[1]&cpage=$matches[2]\";s:33:\"services/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?services=$matches[1]&page=$matches[2]\";s:25:\"services/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"services/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"services/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"services/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"services/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"services/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=32&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}','yes');
INSERT INTO `wp_options` VALUES (30,'hack_file','0','yes');
INSERT INTO `wp_options` VALUES (31,'blog_charset','UTF-8','yes');
INSERT INTO `wp_options` VALUES (32,'moderation_keys','','no');
INSERT INTO `wp_options` VALUES (33,'active_plugins','a:3:{i:0;s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:35:\"message-display/message-display.php\";}','yes');
INSERT INTO `wp_options` VALUES (34,'category_base','','yes');
INSERT INTO `wp_options` VALUES (35,'ping_sites','http://rpc.pingomatic.com/','yes');
INSERT INTO `wp_options` VALUES (36,'comment_max_links','2','yes');
INSERT INTO `wp_options` VALUES (37,'gmt_offset','0','yes');
INSERT INTO `wp_options` VALUES (38,'default_email_category','1','yes');
INSERT INTO `wp_options` VALUES (39,'recently_edited','a:2:{i:0;s:87:\"C:\\Users\\yasmi\\Local Sites\\testdevwp1\\app\\public/wp-content/plugins/akismet/akismet.php\";i:1;s:0:\"\";}','no');
INSERT INTO `wp_options` VALUES (40,'template','testdevwp','yes');
INSERT INTO `wp_options` VALUES (41,'stylesheet','testdevwp','yes');
INSERT INTO `wp_options` VALUES (42,'comment_registration','0','yes');
INSERT INTO `wp_options` VALUES (43,'html_type','text/html','yes');
INSERT INTO `wp_options` VALUES (44,'use_trackback','0','yes');
INSERT INTO `wp_options` VALUES (45,'default_role','subscriber','yes');
INSERT INTO `wp_options` VALUES (46,'db_version','53496','yes');
INSERT INTO `wp_options` VALUES (47,'uploads_use_yearmonth_folders','1','yes');
INSERT INTO `wp_options` VALUES (48,'upload_path','','yes');
INSERT INTO `wp_options` VALUES (49,'blog_public','1','yes');
INSERT INTO `wp_options` VALUES (50,'default_link_category','2','yes');
INSERT INTO `wp_options` VALUES (51,'show_on_front','page','yes');
INSERT INTO `wp_options` VALUES (52,'tag_base','','yes');
INSERT INTO `wp_options` VALUES (53,'show_avatars','1','yes');
INSERT INTO `wp_options` VALUES (54,'avatar_rating','G','yes');
INSERT INTO `wp_options` VALUES (55,'upload_url_path','','yes');
INSERT INTO `wp_options` VALUES (56,'thumbnail_size_w','150','yes');
INSERT INTO `wp_options` VALUES (57,'thumbnail_size_h','150','yes');
INSERT INTO `wp_options` VALUES (58,'thumbnail_crop','1','yes');
INSERT INTO `wp_options` VALUES (59,'medium_size_w','300','yes');
INSERT INTO `wp_options` VALUES (60,'medium_size_h','300','yes');
INSERT INTO `wp_options` VALUES (61,'avatar_default','mystery','yes');
INSERT INTO `wp_options` VALUES (62,'large_size_w','1024','yes');
INSERT INTO `wp_options` VALUES (63,'large_size_h','1024','yes');
INSERT INTO `wp_options` VALUES (64,'image_default_link_type','none','yes');
INSERT INTO `wp_options` VALUES (65,'image_default_size','','yes');
INSERT INTO `wp_options` VALUES (66,'image_default_align','','yes');
INSERT INTO `wp_options` VALUES (67,'close_comments_for_old_posts','0','yes');
INSERT INTO `wp_options` VALUES (68,'close_comments_days_old','14','yes');
INSERT INTO `wp_options` VALUES (69,'thread_comments','1','yes');
INSERT INTO `wp_options` VALUES (70,'thread_comments_depth','5','yes');
INSERT INTO `wp_options` VALUES (71,'page_comments','0','yes');
INSERT INTO `wp_options` VALUES (72,'comments_per_page','50','yes');
INSERT INTO `wp_options` VALUES (73,'default_comments_page','newest','yes');
INSERT INTO `wp_options` VALUES (74,'comment_order','asc','yes');
INSERT INTO `wp_options` VALUES (75,'sticky_posts','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (76,'widget_categories','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (77,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (78,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (79,'uninstall_plugins','a:0:{}','no');
INSERT INTO `wp_options` VALUES (80,'timezone_string','Europe/Paris','yes');
INSERT INTO `wp_options` VALUES (81,'page_for_posts','0','yes');
INSERT INTO `wp_options` VALUES (82,'page_on_front','32','yes');
INSERT INTO `wp_options` VALUES (83,'default_post_format','0','yes');
INSERT INTO `wp_options` VALUES (84,'link_manager_enabled','0','yes');
INSERT INTO `wp_options` VALUES (85,'finished_splitting_shared_terms','1','yes');
INSERT INTO `wp_options` VALUES (86,'site_icon','0','yes');
INSERT INTO `wp_options` VALUES (87,'medium_large_size_w','768','yes');
INSERT INTO `wp_options` VALUES (88,'medium_large_size_h','0','yes');
INSERT INTO `wp_options` VALUES (89,'wp_page_for_privacy_policy','3','yes');
INSERT INTO `wp_options` VALUES (90,'show_comments_cookies_opt_in','1','yes');
INSERT INTO `wp_options` VALUES (91,'admin_email_lifespan','1699169666','yes');
INSERT INTO `wp_options` VALUES (92,'disallowed_keys','','no');
INSERT INTO `wp_options` VALUES (93,'comment_previously_approved','1','yes');
INSERT INTO `wp_options` VALUES (94,'auto_plugin_theme_update_emails','a:0:{}','no');
INSERT INTO `wp_options` VALUES (95,'auto_update_core_dev','enabled','yes');
INSERT INTO `wp_options` VALUES (96,'auto_update_core_minor','enabled','yes');
INSERT INTO `wp_options` VALUES (97,'auto_update_core_major','enabled','yes');
INSERT INTO `wp_options` VALUES (98,'wp_force_deactivated_plugins','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (99,'initial_db_version','53496','yes');
INSERT INTO `wp_options` VALUES (100,'wp_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes');
INSERT INTO `wp_options` VALUES (101,'fresh_site','0','yes');
INSERT INTO `wp_options` VALUES (102,'WPLANG','fr_FR','yes');
INSERT INTO `wp_options` VALUES (103,'user_count','1','no');
INSERT INTO `wp_options` VALUES (104,'widget_block','a:6:{i:2;a:1:{s:7:\"content\";s:19:\"<!-- wp:search /-->\";}i:3;a:1:{s:7:\"content\";s:159:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Articles récents</h2><!-- /wp:heading --><!-- wp:latest-posts /--></div><!-- /wp:group -->\";}i:4;a:1:{s:7:\"content\";s:233:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Commentaires récents</h2><!-- /wp:heading --><!-- wp:latest-comments {\"displayAvatar\":false,\"displayDate\":false,\"displayExcerpt\":false} /--></div><!-- /wp:group -->\";}i:5;a:1:{s:7:\"content\";s:146:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Archives</h2><!-- /wp:heading --><!-- wp:archives /--></div><!-- /wp:group -->\";}i:6;a:1:{s:7:\"content\";s:151:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Catégories</h2><!-- /wp:heading --><!-- wp:categories /--></div><!-- /wp:group -->\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (105,'sidebars_widgets','a:2:{s:19:\"wp_inactive_widgets\";a:5:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";i:3;s:7:\"block-5\";i:4;s:7:\"block-6\";}s:13:\"array_version\";i:3;}','yes');
INSERT INTO `wp_options` VALUES (106,'cron','a:8:{i:1684852467;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1684870467;a:4:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1684870485;a:1:{s:21:\"wp_update_user_counts\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1684913667;a:2:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1684913685;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1684913686;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1684918170;a:1:{s:21:\"ai1wm_storage_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes');
INSERT INTO `wp_options` VALUES (107,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (108,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (109,'widget_archives','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (110,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (111,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (112,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (113,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (114,'widget_meta','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (115,'widget_search','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (116,'widget_recent-posts','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (117,'widget_recent-comments','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (118,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (119,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (120,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (122,'recovery_keys','a:1:{s:22:\"dUSPGKka5zOUFCYCxvlD2D\";a:2:{s:10:\"hashed_key\";s:34:\"$P$Bm2RqiIxEJIC3MD8Y8ucHGzC7CQwl7/\";s:10:\"created_at\";i:1684850231;}}','yes');
INSERT INTO `wp_options` VALUES (123,'theme_mods_twentytwentythree','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1683619172;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";}s:9:\"sidebar-2\";a:2:{i:0;s:7:\"block-5\";i:1;s:7:\"block-6\";}}}}','yes');
INSERT INTO `wp_options` VALUES (124,'https_detection_errors','a:1:{s:20:\"https_request_failed\";a:1:{i:0;s:28:\"La demande HTTPS a échoué.\";}}','yes');
INSERT INTO `wp_options` VALUES (141,'can_compress_scripts','0','no');
INSERT INTO `wp_options` VALUES (156,'finished_updating_comment_type','1','yes');
INSERT INTO `wp_options` VALUES (157,'current_theme','testdevwp','yes');
INSERT INTO `wp_options` VALUES (158,'theme_mods_testdevwp','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}','yes');
INSERT INTO `wp_options` VALUES (159,'theme_switched','','yes');
INSERT INTO `wp_options` VALUES (165,'recovery_mode_email_last_sent','1684850231','yes');
INSERT INTO `wp_options` VALUES (228,'recently_activated','a:4:{s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";i:1684229971;s:24:\"wpforms-lite/wpforms.php\";i:1684229962;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:1684229934;s:29:\"acf-extended/acf-extended.php\";i:1683793726;}','yes');
INSERT INTO `wp_options` VALUES (230,'my_custom_text','<p class=\\\"\\\\&quot;paragraph\\\\&quot;\\\">Oat cake pudding pie chocolate cake chocolate bar liquorice sesame snaps wafer. Wafer sweet roll powder cheesecake carrot cake chocolate cake. Gummi bears pudding gummi bears cookie powder. Macaroon pie wafer croissant tart macaroon fruitcake pudding tart. Cake topping donut jelly beans gummies jelly. Pie dessert biscuit cupcake chupa chups sweet roll jujubes. Pastry tart gingerbread chocolate brownie dragée jelly beans macaroon dessert. Lollipop tart bonbon powder brownie dragée. Apple pie apple pie topping bonbon cookie wafer sweet.</p>','yes');
INSERT INTO `wp_options` VALUES (268,'acf_version','6.1.6','yes');
INSERT INTO `wp_options` VALUES (269,'auto_update_plugins','a:1:{i:0;s:30:\"advanced-custom-fields/acf.php\";}','no');
INSERT INTO `wp_options` VALUES (331,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (396,'action_scheduler_hybrid_store_demarkation','41','yes');
INSERT INTO `wp_options` VALUES (397,'schema-ActionScheduler_StoreSchema','6.0.1684221882','yes');
INSERT INTO `wp_options` VALUES (398,'schema-ActionScheduler_LoggerSchema','3.0.1684221882','yes');
INSERT INTO `wp_options` VALUES (399,'wp_mail_smtp_initial_version','3.8.0','no');
INSERT INTO `wp_options` VALUES (400,'wp_mail_smtp_version','3.8.0','no');
INSERT INTO `wp_options` VALUES (401,'wp_mail_smtp','a:16:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:27:\"yasmine.radouani@outlook.fr\";s:9:\"from_name\";s:9:\"testdevwp\";s:6:\"mailer\";s:4:\"smtp\";s:11:\"return_path\";b:0;s:16:\"from_email_force\";b:1;s:15:\"from_name_force\";b:0;}s:4:\"smtp\";a:7:{s:7:\"autotls\";b:1;s:4:\"auth\";b:1;s:4:\"host\";s:16:\"smtp.mailgun.org\";s:4:\"port\";i:587;s:10:\"encryption\";s:3:\"tls\";s:4:\"user\";s:62:\"postmaster@sandbox9cd8f564988640988e606504f28691bd.mailgun.org\";s:4:\"pass\";s:120:\"G5bht+paCUNQgIshg69pUjdwNtd3kPINl2PQrK4yf3Vq4mgc8/04Phlu+zvQcxV6dz8wMLerAQ69Glq0LvywBaNr5/yhndll2GC+Ig46BC9P2LDMTAgWizgN\";}s:7:\"general\";a:1:{s:29:\"summary_report_email_disabled\";b:0;}s:5:\"gmail\";a:5:{s:9:\"client_id\";s:72:\"155439072965-t4ug8t605034ibku5915ffk9cgu01dof.apps.googleusercontent.com\";s:13:\"client_secret\";s:35:\"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\";s:12:\"access_token\";a:0:{}s:13:\"refresh_token\";s:0:\"\";s:20:\"is_setup_wizard_auth\";b:1;}s:9:\"sendlayer\";a:1:{s:7:\"api_key\";s:0:\"\";}s:7:\"smtpcom\";a:2:{s:7:\"api_key\";s:0:\"\";s:7:\"channel\";s:0:\"\";}s:10:\"sendinblue\";a:2:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";}s:7:\"mailgun\";a:3:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";s:6:\"region\";s:2:\"US\";}s:8:\"sendgrid\";a:2:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";}s:9:\"sparkpost\";a:2:{s:7:\"api_key\";s:0:\"\";s:6:\"region\";s:2:\"US\";}s:8:\"postmark\";a:2:{s:16:\"server_api_token\";s:0:\"\";s:14:\"message_stream\";s:0:\"\";}s:9:\"amazonses\";a:3:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";s:6:\"region\";s:9:\"us-east-1\";}s:7:\"outlook\";a:5:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";s:12:\"access_token\";a:0:{}s:13:\"refresh_token\";s:0:\"\";s:12:\"user_details\";a:1:{s:5:\"email\";s:0:\"\";}}s:4:\"zoho\";a:6:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";s:6:\"domain\";s:3:\"com\";s:12:\"access_token\";a:0:{}s:13:\"refresh_token\";s:0:\"\";s:12:\"user_details\";a:1:{s:5:\"email\";s:0:\"\";}}s:4:\"logs\";a:5:{s:7:\"enabled\";b:0;s:17:\"log_email_content\";b:0;s:16:\"save_attachments\";b:0;s:19:\"open_email_tracking\";b:0;s:19:\"click_link_tracking\";b:0;}s:11:\"alert_email\";a:2:{s:7:\"enabled\";b:0;s:11:\"connections\";a:0:{}}}','no');
INSERT INTO `wp_options` VALUES (402,'wp_mail_smtp_activated_time','1684221882','no');
INSERT INTO `wp_options` VALUES (403,'wp_mail_smtp_activated','a:1:{s:4:\"lite\";i:1684221882;}','yes');
INSERT INTO `wp_options` VALUES (406,'action_scheduler_lock_async-request-runner','1684229994','yes');
INSERT INTO `wp_options` VALUES (410,'wp_mail_smtp_migration_version','5','yes');
INSERT INTO `wp_options` VALUES (411,'wp_mail_smtp_debug_events_db_version','1','yes');
INSERT INTO `wp_options` VALUES (412,'wp_mail_smtp_activation_prevent_redirect','1','yes');
INSERT INTO `wp_options` VALUES (413,'wp_mail_smtp_setup_wizard_stats','a:3:{s:13:\"launched_time\";i:1684221892;s:14:\"completed_time\";i:1684226361;s:14:\"was_successful\";b:1;}','no');
INSERT INTO `wp_options` VALUES (437,'wp_mail_smtp_mail_key','A6RJb2xBM9y99uZyqk7TsYAp/MB4BW38clty2JuoyEI=','yes');
INSERT INTO `wp_options` VALUES (442,'wpforms_activation_redirect','1','yes');
INSERT INTO `wp_options` VALUES (443,'wpforms_version','1.8.1.2','yes');
INSERT INTO `wp_options` VALUES (444,'wpforms_version_lite','1.8.1.2','yes');
INSERT INTO `wp_options` VALUES (445,'wpforms_activated','a:1:{s:4:\"lite\";i:1684226336;}','yes');
INSERT INTO `wp_options` VALUES (450,'wpforms_versions_lite','a:7:{s:5:\"1.5.9\";i:0;s:7:\"1.6.7.2\";i:0;s:5:\"1.6.8\";i:0;s:5:\"1.7.5\";i:0;s:7:\"1.7.5.1\";i:0;s:5:\"1.7.7\";i:0;s:7:\"1.8.1.2\";i:1684226348;}','yes');
INSERT INTO `wp_options` VALUES (451,'widget_wpforms-widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (452,'wpforms_settings','a:3:{s:13:\"modern-markup\";s:1:\"1\";s:20:\"modern-markup-is-set\";b:1;s:26:\"modern-markup-hide-setting\";b:1;}','yes');
INSERT INTO `wp_options` VALUES (453,'wpforms_admin_notices','a:1:{s:14:\"review_request\";a:2:{s:4:\"time\";i:1684226348;s:9:\"dismissed\";b:0;}}','yes');
INSERT INTO `wp_options` VALUES (454,'wp_mail_smtp_debug','a:0:{}','no');
INSERT INTO `wp_options` VALUES (455,'wp_mail_smtp_lite_sent_email_counter','2','yes');
INSERT INTO `wp_options` VALUES (456,'wp_mail_smtp_lite_weekly_sent_email_counter','a:1:{i:20;i:2;}','yes');
INSERT INTO `wp_options` VALUES (458,'wpforms_process_forms_locator_status','completed','yes');
INSERT INTO `wp_options` VALUES (460,'wp_mail_smtp_review_notice','a:2:{s:4:\"time\";i:1684226395;s:9:\"dismissed\";b:0;}','yes');
INSERT INTO `wp_options` VALUES (461,'wp_mail_smtp_notifications','a:4:{s:6:\"update\";i:1684226396;s:4:\"feed\";a:0:{}s:6:\"events\";a:0:{}s:9:\"dismissed\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (468,'wpforms_challenge','a:13:{s:6:\"status\";s:0:\"\";s:4:\"step\";i:0;s:7:\"user_id\";i:1;s:7:\"form_id\";i:0;s:10:\"embed_page\";i:0;s:16:\"embed_page_title\";s:0:\"\";s:16:\"started_date_gmt\";s:0:\"\";s:17:\"finished_date_gmt\";s:0:\"\";s:13:\"seconds_spent\";i:0;s:12:\"seconds_left\";i:0;s:13:\"feedback_sent\";b:0;s:19:\"feedback_contact_me\";b:0;s:13:\"window_closed\";s:0:\"\";}','yes');
INSERT INTO `wp_options` VALUES (493,'ai1wm_updater','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (497,'ai1wm_secret_key','kWScwTtrbseE','yes');
INSERT INTO `wp_options` VALUES (498,'ai1wm_backups_labels','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (499,'ai1wm_sites_links','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (500,'nonce_key','<51|_EB??Kj 5Y5)Uga[a2a_~EJbLgln-Hd9yn{^V/P](?7./gA>V5_PqKv4`-t&','no');
INSERT INTO `wp_options` VALUES (501,'nonce_salt','o:hfnlibw:qnG/%M#V)4T}4ch<LUAq:=&cI1Dl%{F|uO-Yt%:h0}%T9</a<5i1q}','no');
INSERT INTO `wp_options` VALUES (502,'ai1wm_status','a:3:{s:4:\"type\";s:4:\"done\";s:5:\"title\";s:41:\"Your site has been imported successfully!\";s:7:\"message\";s:375:\"» <a class=\"ai1wm-no-underline\" href=\"http://localhost:10016/wp-admin/options-permalink.php#submit\" target=\"_blank\">Save permalinks structure</a>. (opens a new window)<br />» <a class=\"ai1wm-no-underline\" href=\"https://wordpress.org/support/view/plugin-reviews/all-in-one-wp-migration?rate=5#postform\" target=\"_blank\">Optionally, review the plugin</a>. (opens a new window)\";}','yes');
INSERT INTO `wp_options` VALUES (503,'swift_performance_plugin_organizer','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (504,'jetpack_active_modules','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (507,'auth_key','x$r0A2l?hzi`iKv_l4&igsmFza|-*i7SZ jJ+Y7ndlnq=Q9.h*!).:=:ST:rvw1K','no');
INSERT INTO `wp_options` VALUES (508,'auth_salt','fad<zM ww!]vmVS9D[i*^hq6&@M7):LOrwrA=gr)Y8;W|rV6XE?Fbp$%MmdzG{eA','no');
INSERT INTO `wp_options` VALUES (509,'logged_in_key','yPbQEJI7PcF9;z[/ePp&_ .Y{lU-g9TYS4G9a*=wj6}b1eBrGV^s{T mxLW#i Ob','no');
INSERT INTO `wp_options` VALUES (510,'logged_in_salt',' R/i_gif^&dagv`Vf?%3LlECs3u*?UA&qkI@[IR1[H;USR1El/y[o4-)c-MKycuL','no');
INSERT INTO `wp_options` VALUES (526,'_site_transient_timeout_browser_3d1dd3a2e1ab9329b4ffc31f3d98ab92','1684845156','no');
INSERT INTO `wp_options` VALUES (527,'_site_transient_browser_3d1dd3a2e1ab9329b4ffc31f3d98ab92','a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:5:\"112.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:32:\"https://www.mozilla.org/firefox/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no');
INSERT INTO `wp_options` VALUES (528,'_site_transient_timeout_php_check_9522db31646a2e4672d744b6f556967b','1684845156','no');
INSERT INTO `wp_options` VALUES (529,'_site_transient_php_check_9522db31646a2e4672d744b6f556967b','a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}','no');
INSERT INTO `wp_options` VALUES (560,'_site_transient_timeout_browser_95e1b7d0ab9086d6b88e9adfaacf07d8','1684912544','no');
INSERT INTO `wp_options` VALUES (561,'_site_transient_browser_95e1b7d0ab9086d6b88e9adfaacf07d8','a:10:{s:4:\"name\";s:7:\"Firefox\";s:7:\"version\";s:5:\"113.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:32:\"https://www.mozilla.org/firefox/\";s:7:\"img_src\";s:44:\"http://s.w.org/images/browsers/firefox.png?1\";s:11:\"img_src_ssl\";s:45:\"https://s.w.org/images/browsers/firefox.png?1\";s:15:\"current_version\";s:2:\"56\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no');
INSERT INTO `wp_options` VALUES (579,'_transient_health-check-site-status-result','{\"good\":13,\"recommended\":4,\"critical\":1}','yes');
INSERT INTO `wp_options` VALUES (581,'_site_transient_ai1wm_last_check_for_updates','1684826293','no');
INSERT INTO `wp_options` VALUES (617,'_transient_timeout_acf_plugin_updates','1684911474','no');
INSERT INTO `wp_options` VALUES (618,'_transient_acf_plugin_updates','a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"6.1.6\";}}','no');
INSERT INTO `wp_options` VALUES (679,'_site_transient_timeout_theme_roots','1684828094','no');
INSERT INTO `wp_options` VALUES (680,'_site_transient_theme_roots','a:2:{s:9:\"testdevwp\";s:7:\"/themes\";s:17:\"twentytwentythree\";s:7:\"/themes\";}','no');
INSERT INTO `wp_options` VALUES (686,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-6.2.2.zip\";s:6:\"locale\";s:5:\"fr_FR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/fr_FR/wordpress-6.2.2.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.2.2\";s:7:\"version\";s:5:\"6.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.2.2-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-6.2.2-partial-0.zip\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.2.2\";s:7:\"version\";s:5:\"6.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:3:\"6.2\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.2.2-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-6.2.2-partial-0.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.2.2-rollback-0.zip\";}s:7:\"current\";s:5:\"6.2.2\";s:7:\"version\";s:5:\"6.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.1\";s:15:\"partial_version\";s:3:\"6.2\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1684827287;s:15:\"version_checked\";s:3:\"6.2\";s:12:\"translations\";a:0:{}}','no');
INSERT INTO `wp_options` VALUES (687,'_site_transient_update_themes','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1684827288;s:7:\"checked\";a:2:{s:9:\"testdevwp\";s:0:\"\";s:17:\"twentytwentythree\";s:3:\"1.1\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:1:{s:17:\"twentytwentythree\";a:6:{s:5:\"theme\";s:17:\"twentytwentythree\";s:11:\"new_version\";s:3:\"1.1\";s:3:\"url\";s:47:\"https://wordpress.org/themes/twentytwentythree/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/theme/twentytwentythree.1.1.zip\";s:8:\"requires\";s:3:\"6.1\";s:12:\"requires_php\";s:3:\"5.6\";}}s:12:\"translations\";a:0:{}}','no');
INSERT INTO `wp_options` VALUES (688,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1684827288;s:8:\"response\";a:0:{}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:4:\"7.74\";s:7:\"updated\";s:19:\"2023-01-12 22:00:15\";s:7:\"package\";s:89:\"https://downloads.wordpress.org/translation/plugin/all-in-one-wp-migration/7.74/fr_FR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:1:{s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:37:\"w.org/plugins/all-in-one-wp-migration\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:6:\"plugin\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:11:\"new_version\";s:4:\"7.74\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/all-in-one-wp-migration/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/all-in-one-wp-migration.7.74.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-256x256.png?rev=2458334\";s:2:\"1x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-128x128.png?rev=2458334\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-1544x500.png?rev=2902233\";s:2:\"1x\";s:78:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-772x250.png?rev=2902233\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"3.3\";}}s:7:\"checked\";a:3:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"6.1.6\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:4:\"7.74\";s:35:\"message-display/message-display.php\";s:0:\"\";}}','no');
INSERT INTO `wp_options` VALUES (697,'services_types_children','a:0:{}','yes');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` VALUES (1,2,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (2,3,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (4,6,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (5,6,'_edit_lock','1683879272:1');
INSERT INTO `wp_postmeta` VALUES (6,7,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (7,7,'_edit_lock','1683708501:1');
INSERT INTO `wp_postmeta` VALUES (8,8,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (9,8,'_edit_lock','1683708489:1');
INSERT INTO `wp_postmeta` VALUES (10,9,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (11,9,'_edit_lock','1683708475:1');
INSERT INTO `wp_postmeta` VALUES (12,10,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (13,10,'_edit_lock','1684759194:1');
INSERT INTO `wp_postmeta` VALUES (14,12,'_wp_attached_file','2023/05/PIZZAMIAMIAM.jpg');
INSERT INTO `wp_postmeta` VALUES (15,12,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:350;s:6:\"height\";i:350;s:4:\"file\";s:24:\"2023/05/PIZZAMIAMIAM.jpg\";s:8:\"filesize\";i:92109;s:5:\"sizes\";a:2:{s:6:\"medium\";a:5:{s:4:\"file\";s:24:\"PIZZAMIAMIAM-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:19273;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:24:\"PIZZAMIAMIAM-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:7276;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (16,6,'_thumbnail_id','12');
INSERT INTO `wp_postmeta` VALUES (18,15,'_wp_attached_file','2023/05/dessert-g3d84afd04_1280.jpg');
INSERT INTO `wp_postmeta` VALUES (19,15,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:1280;s:6:\"height\";i:853;s:4:\"file\";s:35:\"2023/05/dessert-g3d84afd04_1280.jpg\";s:8:\"filesize\";i:234414;s:5:\"sizes\";a:5:{s:6:\"medium\";a:5:{s:4:\"file\";s:35:\"dessert-g3d84afd04_1280-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:14737;}s:5:\"large\";a:5:{s:4:\"file\";s:36:\"dessert-g3d84afd04_1280-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:102162;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:35:\"dessert-g3d84afd04_1280-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:8150;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:35:\"dessert-g3d84afd04_1280-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:65377;}s:17:\"service_thumbnail\";a:5:{s:4:\"file\";s:35:\"dessert-g3d84afd04_1280-350x350.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:30152;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (20,16,'_wp_attached_file','2023/05/birthday-cake-g644efbb4d_1280.jpg');
INSERT INTO `wp_postmeta` VALUES (21,16,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:853;s:6:\"height\";i:1280;s:4:\"file\";s:41:\"2023/05/birthday-cake-g644efbb4d_1280.jpg\";s:8:\"filesize\";i:268101;s:5:\"sizes\";a:5:{s:6:\"medium\";a:5:{s:4:\"file\";s:41:\"birthday-cake-g644efbb4d_1280-200x300.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:14525;}s:5:\"large\";a:5:{s:4:\"file\";s:42:\"birthday-cake-g644efbb4d_1280-682x1024.jpg\";s:5:\"width\";i:682;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:91949;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:41:\"birthday-cake-g644efbb4d_1280-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:7656;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:42:\"birthday-cake-g644efbb4d_1280-768x1152.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1152;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:113096;}s:17:\"service_thumbnail\";a:5:{s:4:\"file\";s:41:\"birthday-cake-g644efbb4d_1280-350x350.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:24896;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (22,7,'_thumbnail_id','16');
INSERT INTO `wp_postmeta` VALUES (23,8,'_thumbnail_id','15');
INSERT INTO `wp_postmeta` VALUES (24,17,'_wp_attached_file','2023/05/food-g923d623fa_1280.jpg');
INSERT INTO `wp_postmeta` VALUES (25,17,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:1280;s:6:\"height\";i:853;s:4:\"file\";s:32:\"2023/05/food-g923d623fa_1280.jpg\";s:8:\"filesize\";i:216870;s:5:\"sizes\";a:5:{s:6:\"medium\";a:5:{s:4:\"file\";s:32:\"food-g923d623fa_1280-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:15156;}s:5:\"large\";a:5:{s:4:\"file\";s:33:\"food-g923d623fa_1280-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:95101;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:32:\"food-g923d623fa_1280-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:8140;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:32:\"food-g923d623fa_1280-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:61603;}s:17:\"service_thumbnail\";a:5:{s:4:\"file\";s:32:\"food-g923d623fa_1280-350x350.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:28509;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (26,18,'_wp_attached_file','2023/05/meal-g98295624e_1280.jpg');
INSERT INTO `wp_postmeta` VALUES (27,18,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:1280;s:6:\"height\";i:853;s:4:\"file\";s:32:\"2023/05/meal-g98295624e_1280.jpg\";s:8:\"filesize\";i:295033;s:5:\"sizes\";a:5:{s:6:\"medium\";a:5:{s:4:\"file\";s:32:\"meal-g98295624e_1280-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:14406;}s:5:\"large\";a:5:{s:4:\"file\";s:33:\"meal-g98295624e_1280-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:109296;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:32:\"meal-g98295624e_1280-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:6846;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:32:\"meal-g98295624e_1280-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:66958;}s:17:\"service_thumbnail\";a:5:{s:4:\"file\";s:32:\"meal-g98295624e_1280-350x350.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:26133;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (28,9,'_thumbnail_id','18');
INSERT INTO `wp_postmeta` VALUES (29,10,'_thumbnail_id','17');
INSERT INTO `wp_postmeta` VALUES (30,10,'color-service','#FF0000');
INSERT INTO `wp_postmeta` VALUES (31,9,'color-service','#080D0D');
INSERT INTO `wp_postmeta` VALUES (32,8,'color-service','#7DA44A');
INSERT INTO `wp_postmeta` VALUES (33,7,'color-service','#FFCE00');
INSERT INTO `wp_postmeta` VALUES (35,6,'color-service','#4AB7CD');
INSERT INTO `wp_postmeta` VALUES (44,10,'price-service','5');
INSERT INTO `wp_postmeta` VALUES (45,6,'price-service','18');
INSERT INTO `wp_postmeta` VALUES (46,12,'_wp_attachment_image_alt','pizza');
INSERT INTO `wp_postmeta` VALUES (49,28,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (50,28,'_edit_lock','1683884384:1');
INSERT INTO `wp_postmeta` VALUES (52,32,'_edit_lock','1684250440:1');
INSERT INTO `wp_postmeta` VALUES (53,35,'_edit_lock','1684757619:1');
INSERT INTO `wp_postmeta` VALUES (54,35,'_wp_page_template','page-templates/page-contact.php');
INSERT INTO `wp_postmeta` VALUES (55,37,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (56,37,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (57,37,'_menu_item_object_id','35');
INSERT INTO `wp_postmeta` VALUES (58,37,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (59,37,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (60,37,'_menu_item_classes','a:1:{i:0;s:0:\"\";}');
INSERT INTO `wp_postmeta` VALUES (61,37,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (62,37,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (63,39,'_edit_lock','1684163684:1');
INSERT INTO `wp_postmeta` VALUES (64,39,'_wp_page_template','page-templates/page-confirm.php');
INSERT INTO `wp_postmeta` VALUES (65,45,'_edit_lock','1684325379:1');
INSERT INTO `wp_postmeta` VALUES (66,45,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (67,45,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (68,45,'_wp_trash_meta_time','1684312477');
INSERT INTO `wp_postmeta` VALUES (69,45,'_wp_desired_post_slug','services-types');
INSERT INTO `wp_postmeta` VALUES (70,47,'_edit_lock','1684754870:1');
INSERT INTO `wp_postmeta` VALUES (71,47,'_wp_page_template','page-templates/page-allServiceTypes.php');
INSERT INTO `wp_postmeta` VALUES (72,49,'_edit_lock','1684313514:1');
INSERT INTO `wp_postmeta` VALUES (73,53,'_wp_attached_file','2023/05/bg3.jpg');
INSERT INTO `wp_postmeta` VALUES (74,53,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:960;s:6:\"height\";i:350;s:4:\"file\";s:15:\"2023/05/bg3.jpg\";s:8:\"filesize\";i:15834;s:5:\"sizes\";a:4:{s:6:\"medium\";a:5:{s:4:\"file\";s:15:\"bg3-300x109.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:109;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:4005;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:15:\"bg3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:2355;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:15:\"bg3-768x280.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:280;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:13732;}s:17:\"service_thumbnail\";a:5:{s:4:\"file\";s:15:\"bg3-350x350.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:6251;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (75,54,'_wp_attached_file','2023/05/bg1.jpg');
INSERT INTO `wp_postmeta` VALUES (76,54,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:372;s:6:\"height\";i:123;s:4:\"file\";s:15:\"2023/05/bg1.jpg\";s:8:\"filesize\";i:3422;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:14:\"bg1-300x99.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:99;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:3877;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:15:\"bg1-150x123.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:123;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:2566;}s:17:\"service_thumbnail\";a:5:{s:4:\"file\";s:15:\"bg1-350x123.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:123;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:4839;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (77,55,'_wp_attached_file','2023/05/bg4.jpg');
INSERT INTO `wp_postmeta` VALUES (78,55,'_wp_attachment_metadata','a:6:{s:5:\"width\";i:1081;s:6:\"height\";i:360;s:4:\"file\";s:15:\"2023/05/bg4.jpg\";s:8:\"filesize\";i:42778;s:5:\"sizes\";a:5:{s:6:\"medium\";a:5:{s:4:\"file\";s:15:\"bg4-300x100.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:6090;}s:5:\"large\";a:5:{s:4:\"file\";s:16:\"bg4-1024x341.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:341;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:28159;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:15:\"bg4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:3988;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:15:\"bg4-768x256.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:256;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:19547;}s:17:\"service_thumbnail\";a:5:{s:4:\"file\";s:15:\"bg4-350x350.jpg\";s:5:\"width\";i:350;s:6:\"height\";i:350;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:10494;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (79,56,'_edit_lock','1684755615:1');
INSERT INTO `wp_postmeta` VALUES (80,56,'_wp_page_template','page-templates/page-all-services.php');
INSERT INTO `wp_postmeta` VALUES (81,56,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (82,56,'_wp_trash_meta_time','1684755764');
INSERT INTO `wp_postmeta` VALUES (83,56,'_wp_desired_post_slug','services');
INSERT INTO `wp_postmeta` VALUES (84,58,'_edit_lock','1684756435:1');
INSERT INTO `wp_postmeta` VALUES (85,58,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (86,58,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (87,58,'_wp_trash_meta_time','1684756446');
INSERT INTO `wp_postmeta` VALUES (88,58,'_wp_desired_post_slug','services');
INSERT INTO `wp_postmeta` VALUES (89,60,'_edit_lock','1684756839:1');
INSERT INTO `wp_postmeta` VALUES (90,60,'_wp_page_template','page-templates/page-allServices.php');
INSERT INTO `wp_postmeta` VALUES (91,60,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (92,60,'_wp_trash_meta_time','1684756842');
INSERT INTO `wp_postmeta` VALUES (93,60,'_wp_desired_post_slug','services');
INSERT INTO `wp_postmeta` VALUES (94,62,'_edit_lock','1684757990:1');
INSERT INTO `wp_postmeta` VALUES (95,62,'_wp_page_template','page-templates/page-allServices.php');
INSERT INTO `wp_postmeta` VALUES (96,2,'_edit_lock','1684757869:1');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2023-05-09 09:34:26','2023-05-09 07:34:26','<!-- wp:paragraph -->\n<p>Bienvenue sur WordPress. Ceci est votre premier article. Modifiez-le ou supprimez-le, puis commencez à écrire !</p>\n<!-- /wp:paragraph -->','Bonjour tout le monde !','','publish','open','open','','bonjour-tout-le-monde','','','2023-05-09 09:34:26','2023-05-09 07:34:26','',0,'http://localhost:10016/?p=1',0,'post','',1);
INSERT INTO `wp_posts` VALUES (2,1,'2023-05-09 09:34:26','2023-05-09 07:34:26','<!-- wp:paragraph -->\n<p>Ceci est une page d’exemple. C’est différent d’un article de blog parce qu’elle restera au même endroit et apparaîtra dans la navigation de votre site (dans la plupart des thèmes). La plupart des gens commencent par une page « À propos » qui les présente aux personnes visitant le site. Cela pourrait ressembler à quelque chose comme cela :</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Bonjour ! Je suis un mécanicien qui aspire à devenir acteur, et voici mon site. J’habite à Bordeaux, j’ai un super chien baptisé Russell, et j’aime la vodka (ainsi qu’être surpris par la pluie soudaine lors de longues balades sur la plage au coucher du soleil).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>…ou quelque chose comme cela :</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>La société 123 Machin Truc a été créée en 1971, et n’a cessé de proposer au public des machins-trucs de qualité depuis lors. Située à Saint-Remy-en-Bouzemont-Saint-Genest-et-Isson, 123 Machin Truc emploie 2 000 personnes, et fabrique toutes sortes de bidules supers pour la communauté bouzemontoise.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>En tant que nouvel utilisateur ou utilisatrice de WordPress, vous devriez vous rendre sur <a href=\"http://localhost:10016/wp-admin/\">votre tableau de bord</a> pour supprimer cette page et créer de nouvelles pages pour votre contenu. Amusez-vous bien !</p>\n<!-- /wp:paragraph -->','Page d’exemple','','publish','closed','open','','page-d-exemple','','','2023-05-09 09:34:26','2023-05-09 07:34:26','',0,'http://localhost:10016/?page_id=2',0,'page','',0);
INSERT INTO `wp_posts` VALUES (3,1,'2023-05-09 09:34:26','2023-05-09 07:34:26','<!-- wp:heading --><h2>Qui sommes-nous ?</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>L’adresse de notre site est : http://localhost:10016.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Commentaires</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>Quand vous laissez un commentaire sur notre site, les données inscrites dans le formulaire de commentaire, ainsi que votre adresse IP et l’agent utilisateur de votre navigateur sont collectés pour nous aider à la détection des commentaires indésirables.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Une chaîne anonymisée créée à partir de votre adresse e-mail (également appelée hash) peut être envoyée au service Gravatar pour vérifier si vous utilisez ce dernier. Les clauses de confidentialité du service Gravatar sont disponibles ici : https://automattic.com/privacy/. Après validation de votre commentaire, votre photo de profil sera visible publiquement à coté de votre commentaire.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Médias</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>Si vous téléversez des images sur le site, nous vous conseillons d’éviter de téléverser des images contenant des données EXIF de coordonnées GPS. Les personnes visitant votre site peuvent télécharger et extraire des données de localisation depuis ces images.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>Si vous déposez un commentaire sur notre site, il vous sera proposé d’enregistrer votre nom, adresse e-mail et site dans des cookies. C’est uniquement pour votre confort afin de ne pas avoir à saisir ces informations si vous déposez un autre commentaire plus tard. Ces cookies expirent au bout d’un an.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Si vous vous rendez sur la page de connexion, un cookie temporaire sera créé afin de déterminer si votre navigateur accepte les cookies. Il ne contient pas de données personnelles et sera supprimé automatiquement à la fermeture de votre navigateur.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Lorsque vous vous connecterez, nous mettrons en place un certain nombre de cookies pour enregistrer vos informations de connexion et vos préférences d’écran. La durée de vie d’un cookie de connexion est de deux jours, celle d’un cookie d’option d’écran est d’un an. Si vous cochez « Se souvenir de moi », votre cookie de connexion sera conservé pendant deux semaines. Si vous vous déconnectez de votre compte, le cookie de connexion sera effacé.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>En modifiant ou en publiant une publication, un cookie supplémentaire sera enregistré dans votre navigateur. Ce cookie ne comprend aucune donnée personnelle. Il indique simplement l’ID de la publication que vous venez de modifier. Il expire au bout d’un jour.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Contenu embarqué depuis d’autres sites</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>Les articles de ce site peuvent inclure des contenus intégrés (par exemple des vidéos, images, articles…). Le contenu intégré depuis d’autres sites se comporte de la même manière que si le visiteur se rendait sur cet autre site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Ces sites web pourraient collecter des données sur vous, utiliser des cookies, embarquer des outils de suivis tiers, suivre vos interactions avec ces contenus embarqués si vous disposez d’un compte connecté sur leur site web.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Utilisation et transmission de vos données personnelles</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>Si vous demandez une réinitialisation de votre mot de passe, votre adresse IP sera incluse dans l’e-mail de réinitialisation.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Durées de stockage de vos données</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>Si vous laissez un commentaire, le commentaire et ses métadonnées sont conservés indéfiniment. Cela permet de reconnaître et approuver automatiquement les commentaires suivants au lieu de les laisser dans la file de modération.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Pour les comptes qui s’inscrivent sur notre site (le cas échéant), nous stockons également les données personnelles indiquées dans leur profil. Tous les comptes peuvent voir, modifier ou supprimer leurs informations personnelles à tout moment (à l’exception de leur identifiant). Les gestionnaires du site peuvent aussi voir et modifier ces informations.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Les droits que vous avez sur vos données</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>Si vous avez un compte ou si vous avez laissé des commentaires sur le site, vous pouvez demander à recevoir un fichier contenant toutes les données personnelles que nous possédons à votre sujet, incluant celles que vous nous avez fournies. Vous pouvez également demander la suppression des données personnelles vous concernant. Cela ne prend pas en compte les données stockées à des fins administratives, légales ou pour des raisons de sécurité.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Où vos données sont envoyées</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Texte suggéré : </strong>Les commentaires des visiteurs peuvent être vérifiés à l’aide d’un service automatisé de détection des commentaires indésirables.</p><!-- /wp:paragraph -->','Politique de confidentialité','','draft','closed','open','','politique-de-confidentialite','','','2023-05-09 09:34:26','2023-05-09 07:34:26','',0,'http://localhost:10016/?page_id=3',0,'page','',0);
INSERT INTO `wp_posts` VALUES (6,1,'2023-05-09 11:47:57','2023-05-09 09:47:57','<div class=\"panelContainer-2B5Fg\">\r\n<div class=\"panelContainerWrapper-kF1_x\">\r\n<div class=\"panelContainerInnerWrapper-1fMu_\">\r\n<div class=\"designspecContent-1QFtu\" data-auto=\"stylesContainer\">\r\n<div>\r\n<div>\r\n<div>\r\n<div class=\"defaultBlock-3WQYo\">\r\n<div class=\"rah-static rah-static--height-auto\" aria-hidden=\"false\">\r\n<div>\r\n<div class=\"defaultBlock-3WQYo\">\r\n<div class=\"contentRect-aVg9o copyableElem-3MDgL fontCopyableElemTab-2p2c_\" tabindex=\"0\" data-auto=\"contentRect\" data-copytype=\"Content\" data-logtype=\"content\" data-fontinfo=\"Montserrat\">\r\n<p class=\"content-1ObEW\" data-auto=\"textContent\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc convallis sem eu scelerisque bibendum. Sed bibendum auctor libero porttitor dignissim. Curabitur id nisi convallis, porta mauris vitae, ultricies nunc. Ut suscipit faucibus tempus. Aenean magna felis, sodales eleifend metus consectetur, bibendum interdum nunc. Cras sit amet malesuada nulla. Proin convallis ultrices cursus. Donec nec malesuada lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos MIAM PIZZA himenaeos. Etiam pellentesque, augue sed auctor convallis, orci sem tristique orci, sit amet sagittis urna tellus venenatis elit. Sed dui velit, scelerisque et nisi nec, maximus blandit enim.</p>\r\n\r\n<h2 class=\"content-1ObEW\" data-auto=\"textContent\">Quisque molestie pretium eros.</h2>\r\n<p class=\"content-1ObEW\" data-auto=\"textContent\">In ornare tellus et est porttitor, ac tincidunt ipsum condimentum. Praesent non dui erat. Suspendisse rhoncus dictum metus, sit amet blandit dolor dictum ut. Duis augue metus, tincidunt id condimentum vel, sollicitudin vel nisl. Curabitur auctor, arcu id ullamcorper rutrum, sapien leo lacinia turpis, quis eleifend enim dolor sit amet purus. Sed neque mauris, posuere non euismod nec, malesuada sit amet sem. Ut quis pharetra massa. Ut posuere ac nisl id pulvinar. Nullam felis mauris, laoreet eget consectetur ac, iaculis et leo. Cras quis dignissim ipsum. Sed vitae nibh ligula. Integer sed nisl finibus, pulvinar massa non, tristique turpis. Vestibulum fermentum ante quis turpis euismod, eu luctus magna ullamcorper.</p>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>','Service 1','','publish','closed','closed','','service-1','','','2023-05-12 10:06:54','2023-05-12 08:06:54','',0,'http://localhost:10016/?post_type=services&#038;p=6',0,'services','',0);
INSERT INTO `wp_posts` VALUES (7,1,'2023-05-09 11:48:16','2023-05-09 09:48:16','<div>\r\n<div>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum illum tenetur quibusdam veritatis adipisci nobis natus fugiat placeat? Pariatur consectetur modi, recusandae ad harum non sequi laudantium velit animi culpa quaerat distinctio id repellat corrupti asperiores dicta, rem eaque inventore eos magni. Qui labore officia doloribus! Quia nobis eum harum ut corrupti ullam eligendi fuga, eveniet labore facere deserunt, quisquam magnam. Esse officia deleniti quidem fugiat eum, nesciunt quaerat numquam magnam. Perferendis dicta voluptatum officia, fugiat explicabo molestias illo nihil temporibus iure? Excepturi, dolor! Rem inventore, maxime accusamus quis consectetur id est quod, suscipit aperiam eligendi iusto, perspiciatis vero alias?</div>\r\n</div>','Service 2','','publish','closed','closed','','service-2','','','2023-05-10 10:50:43','2023-05-10 08:50:43','',0,'http://localhost:10016/?post_type=services&#038;p=7',0,'services','',0);
INSERT INTO `wp_posts` VALUES (8,1,'2023-05-09 11:48:35','2023-05-09 09:48:35','<div>\r\n<div>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo cumque voluptatibus error, voluptates accusantium a recusandae aspernatur impedit, eaque soluta quae commodi assumenda quam. Est dicta pariatur quasi atque fugit sunt ea possimus debitis impedit natus repellendus, assumenda quod porro corrupti illo laboriosam placeat cupiditate tempore aliquid in deleniti! Eaque error veniam nam labore natus quae magni pariatur amet accusantium optio exercitationem laboriosam debitis vitae odit modi deserunt, ab culpa perspiciatis, voluptates nobis? Adipisci magni nesciunt ullam iusto, ratione alias cupiditate modi deleniti quam totam id fugiat, earum aliquid voluptates beatae iure laudantium officia doloremque maxime nihil ipsam. Optio mollitia, aliquid sunt architecto perferendis nostrum unde aspernatur pariatur quas eum cupiditate? Animi quos laudantium nisi ratione nobis ut, in consectetur facere omnis delectus et architecto vero eveniet perspiciatis quis optio ab modi sed aperiam corporis. Sit accusantium architecto quasi dolores! Necessitatibus id dolores debitis autem. Sit, explicabo! Doloribus minus inventore dicta nobis tempora, sit tempore quas dolor voluptatibus tenetur earum facere nostrum ipsum voluptates optio ipsa quasi! Debitis numquam ab eligendi suscipit nesciunt, culpa ut earum reiciendis, quo possimus vel aliquam, fuga quisquam vero porro error consectetur odio harum! Fugit hic rerum ullam ad animi minima repellat possimus, eos distinctio.</div>\r\n</div>','Service 3','','publish','closed','closed','','service-3','','','2023-05-10 10:50:29','2023-05-10 08:50:29','',0,'http://localhost:10016/?post_type=services&#038;p=8',0,'services','',0);
INSERT INTO `wp_posts` VALUES (9,1,'2023-05-09 11:48:56','2023-05-09 09:48:56','<div>\r\n<div>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo cumque voluptatibus error, voluptates accusantium a recusandae aspernatur impedit, eaque soluta quae commodi assumenda quam. Est dicta pariatur quasi atque fugit sunt ea possimus debitis impedit natus repellendus, assumenda quod porro corrupti illo laboriosam placeat cupiditate tempore aliquid in deleniti! Eaque error veniam nam labore natus quae magni pariatur amet accusantium optio exercitationem laboriosam debitis vitae odit modi deserunt, ab culpa perspiciatis, voluptates nobis? Adipisci magni nesciunt ullam iusto, ratione alias cupiditate modi deleniti quam totam id fugiat, earum aliquid voluptates beatae iure laudantium officia doloremque maxime nihil ipsam. Optio mollitia, aliquid sunt architecto perferendis nostrum unde aspernatur pariatur quas eum cupiditate? Animi quos laudantium nisi ratione nobis ut, in consectetur facere omnis delectus et architecto vero eveniet perspiciatis quis optio ab modi sed aperiam corporis. Sit accusantium architecto quasi dolores! Necessitatibus id dolores debitis autem. Sit, explicabo! Doloribus minus inventore dicta nobis tempora, sit tempore quas dolor voluptatibus tenetur earum facere nostrum ipsum voluptates optio ipsa quasi! Debitis numquam ab eligendi suscipit nesciunt, culpa ut earum reiciendis, quo possimus vel aliquam, fuga quisquam vero porro error consectetur odio harum! Fugit hic rerum ullam ad animi minima repellat possimus, eos distinctio.</div>\r\n</div>','Service 4','','publish','closed','closed','','service-4','','','2023-05-10 10:50:14','2023-05-10 08:50:14','',0,'http://localhost:10016/?post_type=services&#038;p=9',0,'services','',0);
INSERT INTO `wp_posts` VALUES (10,1,'2023-05-09 11:49:07','2023-05-09 09:49:07','<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo cumque voluptatibus error, voluptates accusantium a recusandae aspernatur impedit, eaque soluta quae commodi assumenda quam. Est dicta pariatur quasi atque fugit sunt ea possimus debitis impedit natus repellendus, assumenda quod porro corrupti illo laboriosam placeat cupiditate tempore aliquid in deleniti! Eaque error veniam nam labore natus quae magni pariatur amet accusantium optio exercitationem laboriosam debitis vitae odit modi deserunt, ab culpa perspiciatis, voluptates nobis? Adipisci magni nesciunt ullam iusto, ratione alias cupiditate modi deleniti quam totam id fugiat, earum aliquid voluptates beatae iure laudantium officia doloremque maxime nihil ipsam. Optio mollitia, aliquid sunt architecto perferendis nostrum unde aspernatur pariatur quas eum cupiditate? Animi quos laudantium nisi ratione nobis ut, in consectetur facere omnis delectus et architecto vero eveniet perspiciatis quis optio ab modi sed aperiam corporis. Sit accusantium architecto quasi dolores! Necessitatibus id dolores debitis autem. Sit, explicabo! Doloribus minus inventore dicta nobis tempora, sit tempore quas dolor voluptatibus tenetur earum facere nostrum ipsum voluptates optio ipsa quasi! Debitis numquam ab eligendi suscipit nesciunt, culpa ut earum reiciendis, quo possimus vel aliquam, fuga quisquam vero porro error consectetur odio harum! Fugit hic rerum ullam ad animi minima repellat possimus, eos distinctio.</p>\n<!-- /wp:paragraph -->','Service 5','','publish','closed','closed','','service-5','','','2023-05-17 11:03:26','2023-05-17 09:03:26','',0,'http://localhost:10016/?post_type=services&#038;p=10',0,'services','',0);
INSERT INTO `wp_posts` VALUES (11,1,'2023-05-09 13:36:29','2023-05-09 11:36:29','<div class=\"panelContainer-2B5Fg\">\n<div class=\"panelContainerWrapper-kF1_x\">\n<div class=\"panelContainerInnerWrapper-1fMu_\">\n<div class=\"designspecContent-1QFtu\" data-auto=\"stylesContainer\">\n<div>\n<div>\n<div>\n<div class=\"defaultBlock-3WQYo\">\n<div class=\"rah-static rah-static--height-auto\" aria-hidden=\"false\">\n<div>\n<div class=\"defaultBlock-3WQYo\">\n<div class=\"contentRect-aVg9o copyableElem-3MDgL fontCopyableElemTab-2p2c_\" tabindex=\"0\" data-auto=\"contentRect\" data-copytype=\"Content\" data-logtype=\"content\" data-fontinfo=\"Montserrat\">\n<p class=\"content-1ObEW\" data-auto=\"textContent\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc convallis sem eu scelerisque bibendum. Sed bibendum auctor libero porttitor dignissim. Curabitur id nisi convallis, porta mauris vitae, ultricies nunc. Ut suscipit faucibus tempus. Aenean magna felis, sodales eleifend metus consectetur, bibendum interdum nunc. Cras sit amet malesuada nulla. Proin convallis ultrices cursus. Donec nec malesuada lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos MIAM PIZZA himenaeos. Etiam pellentesque, augue sed auctor convallis, orci sem tristique orci, sit amet sagittis urna tellus venenatis elit. Sed dui velit, scelerisque et nisi nec, maximus blandit enim. Quisque molestie pretium eros. In ornare tellus et est porttitor, ac tincidunt ipsum condimentum. Praesent non dui erat. Suspendisse rhoncus dictum metus, sit amet blandit dolor dictum ut. Duis augue metus, tincidunt id condimentum vel, sollicitudin vel nisl. Curabitur auctor, arcu id ullamcorper rutrum, sapien leo lacinia turpis, quis eleifend enim dolor sit amet purus. Sed neque mauris, posuere non euismod nec, malesuada sit amet sem. Ut quis pharetra massa. Ut posuere ac nisl id pulvinar. Nullam felis mauris, laoreet eget consectetur ac, iaculis et leo. Cras quis dignissim ipsum. Sed vitae nibh ligula. Integer sed nisl finibus, pulvinar massa non, tristique turpis. Vestibulum fermentum ante quis turpis euismod, eu luctus magna ullamcorper.</p>\n\n</div>\n\n<hr class=\"divider-1c7Hc infoBlocks-2RAtY\" />\n\n</div>\n</div>\n</div>\n</div>\n</div>\n</div>\n<div data-auto=\"interactionsView\"></div>\n</div>\n</div>\n</div>\n</div>\n</div>','Service 1','','inherit','closed','closed','','6-autosave-v1','','','2023-05-09 13:36:29','2023-05-09 11:36:29','',6,'http://localhost:10016/?p=11',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (12,1,'2023-05-09 14:23:22','2023-05-09 12:23:22','','PIZZAMIAMIAM','','inherit','open','closed','','pizzamiamiam','','','2023-05-10 17:37:42','2023-05-10 15:37:42','',6,'http://localhost:10016/wp-content/uploads/2023/05/PIZZAMIAMIAM.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (14,1,'2023-05-09 16:25:22','2023-05-09 14:25:22','<div>\n<div>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum illum tenetur quibusdam veritatis adipisci nobis natus fugiat placeat? Pariatur consectetur modi, recusandae ad harum non sequi laudantium velit animi culpa quaerat distinctio id repellat corrupti asperiores dicta, rem eaque inventore eos magni. Qui labore officia doloribus! Quia nobis eum harum ut corrupti ullam eligendi fuga, eveniet labore facere deserunt, quisquam magnam. Esse officia deleniti quidem fugiat eum, nesciunt quaerat numquam magnam. Perferendis dicta voluptatum officia, fugiat explicabo molestias illo nihil temporibus iure? Excepturi, dolor! Rem inventore, maxime accusamus quis consectetur id est quod, suscipit aperiam eligendi iusto, perspiciatis vero alias?</div>\n</div>','Service 2','','inherit','closed','closed','','7-autosave-v1','','','2023-05-09 16:25:22','2023-05-09 14:25:22','',7,'http://localhost:10016/?p=14',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (15,1,'2023-05-09 16:26:17','2023-05-09 14:26:17','','dessert-g3d84afd04_1280','','inherit','open','closed','','dessert-g3d84afd04_1280','','','2023-05-09 16:26:17','2023-05-09 14:26:17','',7,'http://localhost:10016/wp-content/uploads/2023/05/dessert-g3d84afd04_1280.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (16,1,'2023-05-09 16:26:23','2023-05-09 14:26:23','','birthday-cake-g644efbb4d_1280','','inherit','open','closed','','birthday-cake-g644efbb4d_1280','','','2023-05-09 16:26:23','2023-05-09 14:26:23','',7,'http://localhost:10016/wp-content/uploads/2023/05/birthday-cake-g644efbb4d_1280.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (17,1,'2023-05-09 16:28:50','2023-05-09 14:28:50','','food-g923d623fa_1280','','inherit','open','closed','','food-g923d623fa_1280','','','2023-05-09 16:28:50','2023-05-09 14:28:50','',9,'http://localhost:10016/wp-content/uploads/2023/05/food-g923d623fa_1280.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (18,1,'2023-05-09 16:28:54','2023-05-09 14:28:54','','meal-g98295624e_1280','','inherit','open','closed','','meal-g98295624e_1280','','','2023-05-09 16:28:54','2023-05-09 14:28:54','',9,'http://localhost:10016/wp-content/uploads/2023/05/meal-g98295624e_1280.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (19,1,'2023-05-10 10:50:32','2023-05-10 08:50:32','<div>\n<div>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo cumque voluptatibus error, voluptates accusantium a recusandae aspernatur impedit, eaque soluta quae commodi assumenda quam. Est dicta pariatur quasi atque fugit sunt ea possimus debitis impedit natus repellendus, assumenda quod porro corrupti illo laboriosam placeat cupiditate tempore aliquid in deleniti! Eaque error veniam nam labore natus quae magni pariatur amet accusantium optio exercitationem laboriosam debitis vitae odit modi deserunt, ab culpa perspiciatis, voluptates nobis? Adipisci magni nesciunt ullam iusto, ratione alias cupiditate modi deleniti quam totam id fugiat, earum aliquid voluptates beatae iure laudantium officia doloremque maxime nihil ipsam. Optio mollitia, aliquid sunt architecto perferendis nostrum unde aspernatur pariatur quas eum cupiditate? Animi quos laudantium nisi ratione nobis ut, in consectetur facere omnis delectus et architecto vero eveniet perspiciatis quis optio ab modi sed aperiam corporis. Sit accusantium architecto quasi dolores! Necessitatibus id dolores debitis autem. Sit, explicabo! Doloribus minus inventore dicta nobis tempora, sit tempore quas dolor voluptatibus tenetur earum facere nostrum ipsum voluptates optio ipsa quasi! Debitis numquam ab eligendi suscipit nesciunt, culpa ut earum reiciendis, quo possimus vel aliquam, fuga quisquam vero porro error consectetur odio harum! Fugit hic rerum ullam ad animi minima repellat possimus, eos distinctio.</div>\n</div>','Service 3','','inherit','closed','closed','','8-autosave-v1','','','2023-05-10 10:50:32','2023-05-10 08:50:32','',8,'http://localhost:10016/?p=19',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (28,1,'2023-05-12 10:20:28','2023-05-12 08:20:28','a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:12:\"show_in_rest\";i:0;}','Citation','citation','publish','closed','closed','','group_645df5fbd675d','','','2023-05-12 10:20:28','2023-05-12 08:20:28','',0,'http://localhost:10016/?post_type=acf-field-group&#038;p=28',0,'acf-field-group','',0);
INSERT INTO `wp_posts` VALUES (29,1,'2023-05-12 10:20:28','2023-05-12 08:20:28','a:11:{s:10:\"aria-label\";s:0:\"\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}','Auteur de la citation','auteur_de_la_citation','publish','closed','closed','','field_645df5fcb7034','','','2023-05-12 10:20:28','2023-05-12 08:20:28','',28,'http://localhost:10016/?post_type=acf-field&p=29',0,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (30,1,'2023-05-12 10:20:28','2023-05-12 08:20:28','a:11:{s:10:\"aria-label\";s:0:\"\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}','Texte de la citation','texte_de_la_citation','publish','closed','closed','','field_645df673b7035','','','2023-05-12 10:20:28','2023-05-12 08:20:28','',28,'http://localhost:10016/?post_type=acf-field&p=30',1,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (32,1,'2023-05-12 11:46:53','2023-05-12 09:46:53','<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"Liquorrrrrrice dessert chocolate marshmallow dragée. Fruitcake gingerbread jujubes jelly-o cotton candy cupcake ice cream ice cream jelly-o. Lollipop muffin cake chocolate bar chocolate cake cookie chocolate bar croissant. Carrot cake soufflé toffee sugar plum apple pie cotton candy cake. \\u003cstrong\\u003eTootsie roll jelly croissant sesame snaps tart.\\u003c/strong\\u003e\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"Jean-Pierre Dumoulin\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"edit\"} /-->\n\n<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"preview\"} /-->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','Test by Visionsnouvelles','','publish','closed','closed','','test-by-visionsnouvelles','','','2023-05-16 14:33:50','2023-05-16 12:33:50','',0,'http://localhost:10016/?page_id=32',0,'page','',0);
INSERT INTO `wp_posts` VALUES (33,1,'2023-05-12 11:46:53','2023-05-12 09:46:53','<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"edit\"} /-->\n\n<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"preview\"} /-->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','Test by Visionsnouvelles','','inherit','closed','closed','','32-revision-v1','','','2023-05-12 11:46:53','2023-05-12 09:46:53','',32,'http://localhost:10016/?p=33',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (34,1,'2023-05-12 11:56:21','2023-05-12 09:56:21','<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"Coucou la vie est belle !\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"Pierre Dumoulin\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"edit\"} /-->\n\n<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"preview\"} /-->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','Test by Visionsnouvelles','','inherit','closed','closed','','32-revision-v1','','','2023-05-12 11:56:21','2023-05-12 09:56:21','',32,'http://localhost:10016/?p=34',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (35,1,'2023-05-12 15:14:07','2023-05-12 13:14:07','','Page de contact','','publish','closed','closed','','page-de-contact','','','2023-05-22 14:13:29','2023-05-22 12:13:29','',0,'http://localhost:10016/?page_id=35',0,'page','',0);
INSERT INTO `wp_posts` VALUES (36,1,'2023-05-12 15:14:07','2023-05-12 13:14:07','','Page de contact','','inherit','closed','closed','','35-revision-v1','','','2023-05-12 15:14:07','2023-05-12 13:14:07','',35,'http://localhost:10016/?p=36',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (37,1,'2023-05-12 15:21:20','2023-05-12 13:21:20',' ','','','publish','closed','closed','','37','','','2023-05-12 15:21:20','2023-05-12 13:21:20','',0,'http://localhost:10016/?p=37',1,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (38,1,'2023-05-15 12:03:35','2023-05-15 10:03:35','<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"Coucou la vie est belle\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"Pierre Dumoulin\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"edit\"} /-->\n\n<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"preview\"} /-->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','Test by Visionsnouvelles','','inherit','closed','closed','','32-revision-v1','','','2023-05-15 12:03:35','2023-05-15 10:03:35','',32,'http://localhost:10016/?p=38',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (39,1,'2023-05-15 14:56:59','2023-05-15 12:56:59','','Votre demande a été envoyée par e-mail','','publish','closed','closed','','confirmation','','','2023-05-15 14:59:36','2023-05-15 12:59:36','',0,'http://localhost:10016/?page_id=39',0,'page','',0);
INSERT INTO `wp_posts` VALUES (40,1,'2023-05-15 14:56:59','2023-05-15 12:56:59','','Votre demande a été envoyée par e-mail','','inherit','closed','closed','','39-revision-v1','','','2023-05-15 14:56:59','2023-05-15 12:56:59','',39,'http://localhost:10016/?p=40',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (41,1,'2023-05-16 13:06:19','2023-05-16 11:06:19','<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"Liquorice dessert chocolate marshmallow dragée. Fruitcake gingerbread jujubes jelly-o cotton candy cupcake ice cream ice cream jelly-o. Lollipop muffin cake chocolate bar chocolate cake cookie chocolate bar croissant. Carrot cake soufflé toffee sugar plum apple pie cotton candy cake. Tootsie roll jelly croissant sesame snaps tart.\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"Pierre Dumoulin\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"edit\"} /-->\n\n<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"preview\"} /-->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','Test by Visionsnouvelles','','inherit','closed','closed','','32-revision-v1','','','2023-05-16 13:06:19','2023-05-16 11:06:19','',32,'http://localhost:10016/?p=41',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (42,1,'2023-05-16 13:10:10','2023-05-16 11:10:10','<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"Liquorice dessert chocolate marshmallow dragée. Fruitcake gingerbread jujubes jelly-o cotton candy cupcake ice cream ice cream jelly-o. Lollipop muffin cake chocolate bar chocolate cake cookie chocolate bar croissant. Carrot cake soufflé toffee sugar plum apple pie cotton candy cake. \\u003cstrong\\u003eTootsie roll jelly croissant sesame snaps tart.\\u003c/strong\\u003e\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"Pierre Dumoulin\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"edit\"} /-->\n\n<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"preview\"} /-->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','Test by Visionsnouvelles','','inherit','closed','closed','','32-revision-v1','','','2023-05-16 13:10:10','2023-05-16 11:10:10','',32,'http://localhost:10016/?p=42',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (43,1,'2023-05-16 14:32:36','0000-00-00 00:00:00','','Brouillon auto','','auto-draft','open','open','','','','','2023-05-16 14:32:36','0000-00-00 00:00:00','',0,'http://localhost:10016/?p=43',0,'post','',0);
INSERT INTO `wp_posts` VALUES (44,1,'2023-05-16 14:33:50','2023-05-16 12:33:50','<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"Liquorrrrrrice dessert chocolate marshmallow dragée. Fruitcake gingerbread jujubes jelly-o cotton candy cupcake ice cream ice cream jelly-o. Lollipop muffin cake chocolate bar chocolate cake cookie chocolate bar croissant. Carrot cake soufflé toffee sugar plum apple pie cotton candy cake. \\u003cstrong\\u003eTootsie roll jelly croissant sesame snaps tart.\\u003c/strong\\u003e\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"Jean-Pierre Dumoulin\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"edit\"} /-->\n\n<!-- wp:acf/testdevwp-quote {\"name\":\"acf/testdevwp-quote\",\"data\":{\"content_texte\":\"\",\"_content_texte\":\"field_testdevwp-quote_texte\",\"content_auteur\":\"\",\"_content_auteur\":\"field_testdevwp-quote_auteur\",\"content\":\"\",\"_content\":\"field_testdevwp-quote_content\"},\"mode\":\"preview\"} /-->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->','Test by Visionsnouvelles','','inherit','closed','closed','','32-revision-v1','','','2023-05-16 14:33:50','2023-05-16 12:33:50','',32,'http://localhost:10016/?p=44',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (45,1,'2023-05-17 09:29:42','2023-05-17 07:29:42','','Tous les types de services','','trash','closed','closed','','services-types__trashed','','','2023-05-17 10:34:37','2023-05-17 08:34:37','',0,'http://localhost:10016/?page_id=45',0,'page','',0);
INSERT INTO `wp_posts` VALUES (46,1,'2023-05-17 09:29:42','2023-05-17 07:29:42','','Tous les types de services','','inherit','closed','closed','','45-revision-v1','','','2023-05-17 09:29:42','2023-05-17 07:29:42','',45,'http://localhost:10016/?p=46',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (47,1,'2023-05-17 10:35:48','2023-05-17 08:35:48','','Tous les types de services','','publish','closed','closed','','services-types','','','2023-05-17 10:35:48','2023-05-17 08:35:48','',0,'http://localhost:10016/?page_id=47',0,'page','',0);
INSERT INTO `wp_posts` VALUES (48,1,'2023-05-17 10:35:48','2023-05-17 08:35:48','','Tous les types de services','','inherit','closed','closed','','47-revision-v1','','','2023-05-17 10:35:48','2023-05-17 08:35:48','',47,'http://localhost:10016/?p=48',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (49,1,'2023-05-17 10:53:43','0000-00-00 00:00:00','','Brouillon auto','','auto-draft','open','open','','','','','2023-05-17 10:53:43','0000-00-00 00:00:00','',0,'http://localhost:10016/?p=49',0,'post','',0);
INSERT INTO `wp_posts` VALUES (50,1,'2023-05-17 10:59:59','0000-00-00 00:00:00','','Brouillon auto','','auto-draft','closed','closed','','','','','2023-05-17 10:59:59','0000-00-00 00:00:00','',0,'http://localhost:10016/?post_type=services&p=50',0,'services','',0);
INSERT INTO `wp_posts` VALUES (51,1,'2023-05-17 10:59:59','0000-00-00 00:00:00','','Brouillon auto','','auto-draft','closed','closed','','','','','2023-05-17 10:59:59','0000-00-00 00:00:00','',0,'http://localhost:10016/?post_type=services&p=51',0,'services','',0);
INSERT INTO `wp_posts` VALUES (53,1,'2023-05-22 11:20:48','2023-05-22 09:20:48','','bg3','','inherit','open','closed','','bg3','','','2023-05-22 11:20:48','2023-05-22 09:20:48','',0,'http://localhost:10016/wp-content/uploads/2023/05/bg3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (54,1,'2023-05-22 11:20:51','2023-05-22 09:20:51','','bg1','','inherit','open','closed','','bg1','','','2023-05-22 11:20:51','2023-05-22 09:20:51','',0,'http://localhost:10016/wp-content/uploads/2023/05/bg1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (55,1,'2023-05-22 11:22:23','2023-05-22 09:22:23','','bg4','','inherit','open','closed','','bg4','','','2023-05-22 11:22:23','2023-05-22 09:22:23','',0,'http://localhost:10016/wp-content/uploads/2023/05/bg4.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (56,1,'2023-05-22 13:31:26','2023-05-22 11:31:26','','Tous les services','','trash','closed','closed','','services__trashed','','','2023-05-22 13:42:44','2023-05-22 11:42:44','',0,'http://localhost:10016/?page_id=56',0,'page','',0);
INSERT INTO `wp_posts` VALUES (57,1,'2023-05-22 13:31:26','2023-05-22 11:31:26','','Tous les services','','inherit','closed','closed','','56-revision-v1','','','2023-05-22 13:31:26','2023-05-22 11:31:26','',56,'http://localhost:10016/?p=57',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (58,1,'2023-05-22 13:43:36','2023-05-22 11:43:36','','Tous les services','','trash','closed','closed','','services__trashed-2','','','2023-05-22 13:54:06','2023-05-22 11:54:06','',0,'http://localhost:10016/?page_id=58',0,'page','',0);
INSERT INTO `wp_posts` VALUES (59,1,'2023-05-22 13:43:36','2023-05-22 11:43:36','','Tous les services','','inherit','closed','closed','','58-revision-v1','','','2023-05-22 13:43:36','2023-05-22 11:43:36','',58,'http://localhost:10016/?p=59',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (60,1,'2023-05-22 13:55:14','2023-05-22 11:55:14','','Tous les services','','trash','closed','closed','','services__trashed-3','','','2023-05-22 14:00:42','2023-05-22 12:00:42','',0,'http://localhost:10016/?page_id=60',0,'page','',0);
INSERT INTO `wp_posts` VALUES (61,1,'2023-05-22 13:55:14','2023-05-22 11:55:14','','Tous les services','','inherit','closed','closed','','60-revision-v1','','','2023-05-22 13:55:14','2023-05-22 11:55:14','',60,'http://localhost:10016/?p=61',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (62,1,'2023-05-22 14:01:31','2023-05-22 12:01:31','<!-- wp:paragraph -->\n<p>Voici tous les services !</p>\n<!-- /wp:paragraph -->','Tous les services','','publish','closed','closed','','services','','','2023-05-22 14:14:24','2023-05-22 12:14:24','',0,'http://localhost:10016/?page_id=62',0,'page','',0);
INSERT INTO `wp_posts` VALUES (63,1,'2023-05-22 14:01:31','2023-05-22 12:01:31','','Tous les services','','inherit','closed','closed','','62-revision-v1','','','2023-05-22 14:01:31','2023-05-22 12:01:31','',62,'http://localhost:10016/?p=63',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (64,1,'2023-05-22 14:14:24','2023-05-22 12:14:24','<!-- wp:paragraph -->\n<p>Voici tous les services !</p>\n<!-- /wp:paragraph -->','Tous les services','','inherit','closed','closed','','62-revision-v1','','','2023-05-22 14:14:24','2023-05-22 12:14:24','',62,'http://localhost:10016/?p=64',0,'revision','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` VALUES (1,1,0);
INSERT INTO `wp_term_relationships` VALUES (6,2,0);
INSERT INTO `wp_term_relationships` VALUES (7,3,0);
INSERT INTO `wp_term_relationships` VALUES (8,2,0);
INSERT INTO `wp_term_relationships` VALUES (8,3,0);
INSERT INTO `wp_term_relationships` VALUES (9,3,0);
INSERT INTO `wp_term_relationships` VALUES (10,4,0);
INSERT INTO `wp_term_relationships` VALUES (37,5,0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (2,2,'services_types','Service cool',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (3,3,'services_types','Service fun',0,3);
INSERT INTO `wp_term_taxonomy` VALUES (4,4,'services_types','Service relou',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (5,5,'nav_menu','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (9,9,'services_types','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (10,10,'services_types','',0,0);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_termmeta`
--

LOCK TABLES `wp_termmeta` WRITE;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
INSERT INTO `wp_termmeta` VALUES (1,4,'services_types_image','http://localhost:10016/wp-content/uploads/2023/05/bg1.jpg');
INSERT INTO `wp_termmeta` VALUES (2,3,'services_types_image','http://localhost:10016/wp-content/uploads/2023/05/bg3.jpg');
INSERT INTO `wp_termmeta` VALUES (6,2,'services_types_image','http://localhost:10016/wp-content/uploads/2023/05/bg4.jpg');
INSERT INTO `wp_termmeta` VALUES (7,9,'services_types_image','http://localhost:10016/wp-content/uploads/2023/05/meal-g98295624e_1280.jpg');
INSERT INTO `wp_termmeta` VALUES (8,10,'services_types_image','17');
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Non classé','non-classe',0);
INSERT INTO `wp_terms` VALUES (2,'Service cool','service_cool',0);
INSERT INTO `wp_terms` VALUES (3,'Service fun','service_fun',0);
INSERT INTO `wp_terms` VALUES (4,'Service relou','service_relou',0);
INSERT INTO `wp_terms` VALUES (5,'menu','menu',0);
INSERT INTO `wp_terms` VALUES (9,'service raleur','service-raleur',0);
INSERT INTO `wp_terms` VALUES (10,'service 2','service-2',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'nickname','admin');
INSERT INTO `wp_usermeta` VALUES (2,1,'first_name','');
INSERT INTO `wp_usermeta` VALUES (3,1,'last_name','');
INSERT INTO `wp_usermeta` VALUES (4,1,'description','');
INSERT INTO `wp_usermeta` VALUES (5,1,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (6,1,'syntax_highlighting','true');
INSERT INTO `wp_usermeta` VALUES (7,1,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (8,1,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (9,1,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (10,1,'show_admin_bar_front','true');
INSERT INTO `wp_usermeta` VALUES (11,1,'locale','');
INSERT INTO `wp_usermeta` VALUES (12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (13,1,'wp_user_level','10');
INSERT INTO `wp_usermeta` VALUES (14,1,'dismissed_wp_pointers','plugin_editor_notice');
INSERT INTO `wp_usermeta` VALUES (15,1,'show_welcome_panel','1');
INSERT INTO `wp_usermeta` VALUES (16,1,'session_tokens','a:3:{s:64:\"bd7090c7758c03cd469881961e17a3f901d7c1beb3b1534e1af9e18f8d1bfc16\";a:4:{s:10:\"expiration\";i:1684827285;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:80:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/112.0\";s:5:\"login\";i:1683617685;}s:64:\"fc525162cfbb66f3702f7fb41dd2e719ed75c852fb319500b7f8c1a47f0d09f7\";a:4:{s:10:\"expiration\";i:1684403412;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:80:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/112.0\";s:5:\"login\";i:1684230612;}s:64:\"33c2f5157d1bbf1e9580553f1dde177114a8dc19d190284a0128897f0020c532\";a:4:{s:10:\"expiration\";i:1685517339;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:80:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/113.0\";s:5:\"login\";i:1684307739;}}');
INSERT INTO `wp_usermeta` VALUES (17,1,'wp_dashboard_quick_press_last_post_id','43');
INSERT INTO `wp_usermeta` VALUES (18,1,'community-events-location','a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}');
INSERT INTO `wp_usermeta` VALUES (19,1,'wp_user-settings','libraryContent=browse&editor=tinymce');
INSERT INTO `wp_usermeta` VALUES (20,1,'wp_user-settings-time','1683883998');
INSERT INTO `wp_usermeta` VALUES (21,1,'wp_persisted_preferences','a:2:{s:14:\"core/edit-post\";a:3:{s:26:\"isComplementaryAreaVisible\";b:1;s:12:\"welcomeGuide\";b:0;s:10:\"openPanels\";a:3:{i:0;s:11:\"post-status\";i:1;s:12:\"post-excerpt\";i:2;s:15:\"page-attributes\";}}s:9:\"_modified\";s:24:\"2023-05-10T10:06:18.636Z\";}');
INSERT INTO `wp_usermeta` VALUES (22,1,'managenav-menuscolumnshidden','a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}');
INSERT INTO `wp_usermeta` VALUES (23,1,'metaboxhidden_nav-menus','a:3:{i:0;s:22:\"add-post-type-services\";i:1;s:12:\"add-post_tag\";i:2;s:18:\"add-services_types\";}');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (1,'admin','$P$BUQGoEjGsa9tADT88uIZLzBuwbYTgy0','admin','yasmine.radouani@outlook.fr','http://localhost:10016','2023-05-09 07:34:26','',0,'admin');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_wpforms_tasks_meta`
--

DROP TABLE IF EXISTS `wp_wpforms_tasks_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_wpforms_tasks_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_wpforms_tasks_meta`
--

LOCK TABLES `wp_wpforms_tasks_meta` WRITE;
/*!40000 ALTER TABLE `wp_wpforms_tasks_meta` DISABLE KEYS */;
INSERT INTO `wp_wpforms_tasks_meta` VALUES (1,'wpforms_process_forms_locator_scan','W10=','2023-05-16 08:39:08');
INSERT INTO `wp_wpforms_tasks_meta` VALUES (3,'wpforms_admin_addons_cache_update','W10=','2023-05-16 08:46:11');
/*!40000 ALTER TABLE `wp_wpforms_tasks_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_wpmailsmtp_debug_events`
--

DROP TABLE IF EXISTS `wp_wpmailsmtp_debug_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_wpmailsmtp_debug_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `initiator` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `event_type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_wpmailsmtp_debug_events`
--

LOCK TABLES `wp_wpmailsmtp_debug_events` WRITE;
/*!40000 ALTER TABLE `wp_wpmailsmtp_debug_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_wpmailsmtp_debug_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_wpmailsmtp_tasks_meta`
--

DROP TABLE IF EXISTS `wp_wpmailsmtp_tasks_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_wpmailsmtp_tasks_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_wpmailsmtp_tasks_meta`
--

LOCK TABLES `wp_wpmailsmtp_tasks_meta` WRITE;
/*!40000 ALTER TABLE `wp_wpmailsmtp_tasks_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_wpmailsmtp_tasks_meta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-23 16:10:57
